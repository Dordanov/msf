/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anchormen.dordanov.msf.compare;

import java.util.Comparator;

/**
 *
 * @author Nico
 */
public abstract class AbstractComparator<T> implements Comparator<T>
{
	
	protected int nullSafeCompareField(Comparable one, Comparable two) 
	{
		if (one == null && two == null) 
		{
			return 0;
		}
		
		if ( one == null || two == null ) 
		{
			return (one == null) ? -1 : 1;
		}
		
		return one.compareTo(two);
	}
	
}
