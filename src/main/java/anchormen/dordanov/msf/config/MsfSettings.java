package anchormen.dordanov.msf.config;

import anchormen.platform3.config.ConfigManager;

/**
 *
 * @author nico
 */
public class MsfSettings
{

	public static String getDefaultFromAddress()
	{
		return (String) ConfigManager.<Object>getConfig("DefaultFromAddress", ConfigManager.nullInstanceProvider());
	}

	public static String getDefaultFromNaam()
	{
		return (String) ConfigManager.<Object>getConfig("DefaultFromNaam", ConfigManager.nullInstanceProvider());
	}

	public static String getSiteRootUrl()
	{
		return (String) ConfigManager.<Object>getConfig("SiteRootUrl", ConfigManager.nullInstanceProvider());
	}

	public static String getSiteRootUrlZonderContext()
	{
		return (String) ConfigManager.<Object>getConfig("SiteRootUrlZonderContext", ConfigManager.nullInstanceProvider());
	}

	public static String getEnvironment()
	{
		return (String) ConfigManager.<Object>getConfig("Environment", ConfigManager.nullInstanceProvider());
	}
	
}
