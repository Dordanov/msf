/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anchormen.dordanov.msf.converter;

import anchormen.dordanov.msf.converter.converters.DutchDateTimeConverter;
import anchormen.dordanov.msf.converter.converters.NoGroupingIntegerConverter;
import anchormen.platform3.converter.factory.ConverterFactory;
import anchormen.platform3.converter.factory.providers.StandardConverterFactoryProvider;
import java.util.Date;

/**
 *
 * @author Nico
 */
public class MsfConverterFactoryProvider extends StandardConverterFactoryProvider
{

	@Override
	public ConverterFactory createFactory()
	{
		ConverterFactory factory = super.createFactory();

		//Always use Dutch dates, with optional time info.
		factory.registerConverter(Date.class, DutchDateTimeConverter.class);
		//No grouping in Integers please, we use the english format and I hate the grouping with comma's.
		factory.registerConverter(Integer.class, NoGroupingIntegerConverter.class);
		
		return factory;
	}
	
}