/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anchormen.dordanov.msf.converter.converters;

import anchormen.platform3.converter.AbstractConverter;
import anchormen.platform3.converter.ConvertException;
import anchormen.platform3.util.DateTimeUtil;
import anchormen.platform3.util.StringUtil;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author Nico
 */
public class DutchDateTimeConverter extends AbstractConverter
{
    private static final Date MIN_DATE = DateTimeUtil.fromString("yyyy-MM-dd HH:mm", "1000-01-01 00:00");
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy", new Locale("nl-NL"));
	private static final DateFormat DATE_TIME_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm", new Locale("nl-NL"));

    @Override
    public String objectToString(Object object) throws ConvertException
    {
        if (object == null)
        {
            return "";
        }

		Date date = (Date)object;
		
		//is there time info present? -> use date and time.
		if ( date.after(DateTimeUtil.startOfDay(date)) )
			return DATE_TIME_FORMAT.format(date);
		
		//there was no time present? we will just use date.
        return DATE_FORMAT.format(date);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T stringToObject(String object) throws ConvertException
    {
        if (StringUtil.isNullOrEmpty(object))
        {
            return null;
        }
        try
        {
			//is there time info present? -> use date and time.
			Date date;
			if ( object.length() > 10 )
				date = DATE_TIME_FORMAT.parse(object);
			else //no? just the date then.
				date = DATE_FORMAT.parse(object);
				
            if ( date.before(MIN_DATE) )
            {
                throw new ConvertException("Could not parse datetime!");
            }
            return (T) date;
        }
		catch (ParseException e)
        {
            throw new ConvertException("Could not parse datetime! ", e);
        }
    }
	
}