package anchormen.dordanov.msf.converter.converters;

import anchormen.java.util.Strings;
import anchormen.platform3.converter.ConvertException;
import anchormen.platform3.converter.converters.number.IntegerConverter;
import java.text.NumberFormat;
import java.util.Locale;

/**
 *
 * @author n.schouten
 */
public class NoGroupingIntegerConverter extends IntegerConverter 
{
	
	@Override
	protected Number stringToNumber(String string) throws ConvertException
	{		
		if ( Strings.isNullOrEmpty(string) )
			return null;
		
		// Eventuele thousand seperators weghalen.
		// In de applicatie wordt op een hoop plaatsen namelijk een integer gewoon geprint, en daar zitten nog steeds seperators in.
		return super.stringToNumber(string.replace(",", ""));
	}
	
	@Override
	protected NumberFormat craeteNewNumberFormat(Locale locale)
	{
		NumberFormat format = super.craeteNewNumberFormat(locale) ;
		format.setGroupingUsed(false);		
		return format;
	}
}

