package anchormen.dordanov.msf.dao;

import anchormen.dordanov.msf.domain.MarvelCharacterAbilityLevel;
import anchormen.dordanov.msf.domain.search.MarvelCharacterAbilityLevelSearchData;
import anchormen.platform3.dao.searchdata.AbstractSearchDataDao;
import anchormen.platform3.web.tags.table.support.Alias;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.PlatformTransactionManager;

/**
 *
 * @author Nico
 */
public class MarvelCharacterAbilityLevelDao extends AbstractSearchDataDao<MarvelCharacterAbilityLevel, MarvelCharacterAbilityLevelSearchData>
{
	public MarvelCharacterAbilityLevelDao(SessionFactory sessionFactory, PlatformTransactionManager transactionManager)
	{
		super(sessionFactory, transactionManager);
	}

	@Override
	public Criterion createSearchDataCriteria(MarvelCharacterAbilityLevelSearchData sd, List<Alias> list)
	{
		Conjunction junction = Restrictions.conjunction();
		
		addEQPropertyWithNullCheck(junction, "marvelCharacter", sd.getMarvelCharacter());
		addEQPropertyWithNullCheck(junction, "abilityType", sd.getAbilityType());
		
		return junction;
	}

	@Override
	public MarvelCharacterAbilityLevelSearchData createSearchData()
	{
		return new MarvelCharacterAbilityLevelSearchData();
	}
	
}
