package anchormen.dordanov.msf.dao;

import anchormen.dordanov.msf.domain.MarvelCharacter;
import anchormen.platform3.dao.searchdata.AbstractSearchDataDao;
import anchormen.platform3.web.tags.table.support.Alias;
import anchormen.dordanov.msf.domain.search.MarvelCharacterSearchData;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.PlatformTransactionManager;

/**
 *
 * @author n.schouten
 */
public class MarvelCharacterDao extends AbstractSearchDataDao<MarvelCharacter, MarvelCharacterSearchData>
{

	public MarvelCharacterDao(SessionFactory sessionFactory, PlatformTransactionManager transactionManager)
	{
		super(sessionFactory, transactionManager);
	}

	@Override
	public Criterion createSearchDataCriteria(MarvelCharacterSearchData searchData, List<Alias> aliases)
	{
		Conjunction junction = Restrictions.conjunction();
		
		
		return junction;
	}

	@Override
	public MarvelCharacterSearchData createSearchData()
	{
		return new MarvelCharacterSearchData();
	}

	@Override
	public Order[] getDefaultOrder()
	{
		return new Order[]{Order.desc("power"), Order.asc("starRank"), Order.asc("redStarRank"), Order.asc("gearTier")};
	}
	
	
}
