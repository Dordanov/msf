package anchormen.dordanov.msf.dao;

import anchormen.dordanov.msf.domain.Trait;
import anchormen.dordanov.msf.domain.search.TraitSearchData;
import anchormen.platform3.dao.searchdata.AbstractSearchDataDao;
import anchormen.platform3.web.tags.table.support.Alias;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.PlatformTransactionManager;

/**
 *
 * @author Nico
 */
public class TraitDao extends AbstractSearchDataDao<Trait, TraitSearchData>
{
	
	public TraitDao(SessionFactory sessionFactory, PlatformTransactionManager transactionManager)
	{
		super(sessionFactory, transactionManager);
	}

	@Override
	public Criterion createSearchDataCriteria(TraitSearchData sd, List<Alias> list)
	{
		Conjunction junction = Restrictions.conjunction();
		
		addEQPropertyWithNullCheck(junction, "type", sd.getTraitType());
		
		return junction;
	}

	@Override
	public TraitSearchData createSearchData()
	{
		return new TraitSearchData();
	}
	
}
