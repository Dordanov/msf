package anchormen.dordanov.msf.dao;

import anchormen.dordanov.msf.domain.User;
import anchormen.dordanov.msf.domain.search.UserSearchData;
import anchormen.platform3.dao.searchdata.AbstractSearchDataDao;
import anchormen.platform3.web.tags.table.support.Alias;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.PlatformTransactionManager;

/**
 *
 * @author Nico
 */
public class UserDao extends AbstractSearchDataDao<User, UserSearchData>
{

	public UserDao(SessionFactory sessionFactory, PlatformTransactionManager transactionManager)
	{
		super(sessionFactory, transactionManager);
	}

	@Override
	public Criterion createSearchDataCriteria(UserSearchData searchData, List<Alias> aliases)
	{
		Conjunction junction = Restrictions.conjunction();
		
		addEQPropertyWithNullCheck(junction, "emailAddress", searchData.getEmailaddress());
		
		addEQPropertyWithNullCheck(junction, "password", searchData.getPasswordEncrypted());
				
		
		addEQPropertyWithNullCheck(junction, "firstName", searchData.getFirstName());
		addEQPropertyWithNullCheck(junction, "lastName", searchData.getLastName());
		
		return junction;
	}

	@Override
	public UserSearchData createSearchData()
	{
		return new UserSearchData();
	}
	
}
