package anchormen.dordanov.msf.domain;

import anchormen.platform3.domain.AbstractEntity;
import java.util.Set;

/**
 *
 * @author n.schouten
 */
public class MarvelCharacter extends AbstractEntity
{
	private String _name;
	private Integer _level;
	private Integer _starRank;
	private Integer _redStarRank;
	private Integer _gearTier;
	private Integer _power;
	private Set<Trait> _traits;

	public String getName()
	{
		return _name;
	}

	public void setName(String name)
	{
		_name = name;
	}

	public Integer getLevel()
	{
		return _level;
	}

	public void setLevel(Integer level)
	{
		_level = level;
	}

	public Integer getStarRank()
	{
		return _starRank;
	}

	public void setStarRank(Integer starRank)
	{
		_starRank = starRank;
	}

	public Integer getRedStarRank()
	{
		return _redStarRank;
	}

	public void setRedStarRank(Integer redStarRank)
	{
		_redStarRank = redStarRank;
	}

	public Integer getGearTier()
	{
		return _gearTier;
	}

	public void setGearTier(Integer gearTier)
	{
		_gearTier = gearTier;
	}

	public Integer getPower()
	{
		return _power;
	}

	public void setPower(Integer power)
	{
		_power = power;
	}

	public Set<Trait> getTraits()
	{
		return _traits;
	}

	public void setTraits(Set<Trait> traits)
	{
		_traits = traits;
	}
	
}
