package anchormen.dordanov.msf.domain;

import anchormen.dordanov.msf.domain.support.AbilityType;
import anchormen.platform3.domain.AbstractEntity;

/**
 *
 * @author Nico
 */
public class MarvelCharacterAbilityLevel extends AbstractEntity
{
	private MarvelCharacter _marvelCharacter;
	private AbilityType _abilityType;
	private Integer _level;
	private Trait _synergyOne;
	private Trait _synergyTwo;
	private MarvelCharacter _synergyCharacter;

	public MarvelCharacter getMarvelCharacter()
	{
		return _marvelCharacter;
	}

	public void setMarvelCharacter(MarvelCharacter character)
	{
		_marvelCharacter = character;
	}

	public AbilityType getAbilityType()
	{
		return _abilityType;
	}

	public void setAbilityType(AbilityType type)
	{
		_abilityType = type;
	}

	public Integer getLevel()
	{
		return _level;
	}

	public void setLevel(Integer level)
	{
		_level = level;
	}

	public Trait getSynergyOne()
	{
		return _synergyOne;
	}

	public void setSynergyOne(Trait synergyOne)
	{
		_synergyOne = synergyOne;
	}

	public Trait getSynergyTwo()
	{
		return _synergyTwo;
	}

	public void setSynergyTwo(Trait synergyTwo)
	{
		_synergyTwo = synergyTwo;
	}

	public MarvelCharacter getSynergyCharacter()
	{
		return _synergyCharacter;
	}

	public void setSynergyCharacter(MarvelCharacter synergyCharacter)
	{
		_synergyCharacter = synergyCharacter;
	}
	
	
	
}
