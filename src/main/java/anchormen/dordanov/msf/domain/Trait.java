package anchormen.dordanov.msf.domain;

import anchormen.dordanov.msf.domain.support.TraitType;
import anchormen.platform3.domain.AbstractEntity;

/**
 *
 * @author Nico
 */
public class Trait extends AbstractEntity
{
	private String _name;
	private TraitType _type;

	public String getName()
	{
		return _name;
	}

	public void setName(String name)
	{
		_name = name;
	}

	public TraitType getType()
	{
		return _type;
	}

	public void setType(TraitType type)
	{
		_type = type;
	}	
}
