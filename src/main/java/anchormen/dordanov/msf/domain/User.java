package anchormen.dordanov.msf.domain;

import anchormen.java.util.Strings;
import anchormen.platform3.converter.ToLabelSupport;
import anchormen.platform3.domain.AbstractEntity;
import anchormen.platform3.security.web.Authenticatable;

/**
 *
 * @author Nico
 */
public class User extends AbstractEntity implements Authenticatable<Long>, ToLabelSupport
{
	private String _emailAddress;
	private String _password;
	private String _firstName;
	private String _lastName;

	private Boolean _mustChangePassword;
	private Boolean _superUser;

	
	public String getEmailAddress()
	{
		return _emailAddress;
	}

	public void setEmailAddress(String emailAddress)
	{
		this._emailAddress = emailAddress;
	}

	public String getPassword()
	{
		return _password;
	}

	public void setPassword(String password)
	{
		this._password = password;
	}

	public String getFirstName()
	{
		return _firstName;
	}

	public void setFirstName(String firstName)
	{
		this._firstName = firstName;
	}

	public String getLastName()
	{
		return _lastName;
	}

	public void setLastName(String lastName)
	{
		this._lastName = lastName;
	}

	public Boolean getMustChangePassword()
	{
		return _mustChangePassword;
	}

	public void setMustChangePassword(Boolean mustChangePassword)
	{
		this._mustChangePassword = mustChangePassword;
	}

	public Boolean getSuperUser()
	{
		return _superUser;
	}

	public void setSuperUser(Boolean superUser)
	{
		this._superUser = superUser;
	}


	@Override
	public String getName()
	{
		return Strings.joinNonEmpty(" ", getFirstName(), getLastName());
	}

	@Override
	public String toLabel()
	{
		return getName();
	}

}

