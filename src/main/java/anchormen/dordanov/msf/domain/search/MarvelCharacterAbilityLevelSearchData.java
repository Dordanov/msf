package anchormen.dordanov.msf.domain.search;

import anchormen.dordanov.msf.domain.MarvelCharacter;
import anchormen.dordanov.msf.domain.MarvelCharacterAbilityLevel;
import anchormen.dordanov.msf.domain.support.AbilityType;
import anchormen.platform3.domain.search.AbstractSearchData;

/**
 *
 * @author Nico
 */
public class MarvelCharacterAbilityLevelSearchData extends AbstractSearchData<MarvelCharacterAbilityLevel>
{
	private MarvelCharacter _marvelCharacter;
	private AbilityType _abilityType;

	public MarvelCharacter getMarvelCharacter()
	{
		return _marvelCharacter;
	}

	public void setMarvelCharacter(MarvelCharacter marvelCharacter)
	{
		_marvelCharacter = marvelCharacter;
	}

	public AbilityType getAbilityType()
	{
		return _abilityType;
	}

	public void setAbilityType(AbilityType abilityType)
	{
		_abilityType = abilityType;
	}	
}
