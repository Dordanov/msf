package anchormen.dordanov.msf.domain.search;

import anchormen.dordanov.msf.domain.Trait;
import anchormen.dordanov.msf.domain.support.TraitType;
import anchormen.platform3.domain.search.AbstractSearchData;

/**
 *
 * @author Nico
 */
public class TraitSearchData extends AbstractSearchData<Trait>
{
	private TraitType _traitType;

	public TraitType getTraitType()
	{
		return _traitType;
	}

	public void setTraitType(TraitType traitType)
	{
		_traitType = traitType;
	}	
}
