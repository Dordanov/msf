package anchormen.dordanov.msf.domain.search;

import anchormen.dordanov.msf.domain.User;
import anchormen.platform3.domain.search.AbstractSearchData;

/**
 *
 * @author Nico
 */
public class UserSearchData extends AbstractSearchData<User>
{
	private String _emailaddress;
	private String _passwordEncrypted;
	private String _firstName;
	private String _lastName;

	
	
	public String getEmailaddress()
	{
		return _emailaddress;
	}

	public void setEmailaddress(String emailaddress)
	{
		this._emailaddress = emailaddress;
	}

	public String getPasswordEncrypted()
	{
		return _passwordEncrypted;
	}

	public void setPasswordEncrypted(String passwordEncrypted)
	{
		this._passwordEncrypted = passwordEncrypted;
	}

	public String getFirstName()
	{
		return _firstName;
	}

	public void setFirstName(String firstName)
	{
		this._firstName = firstName;
	}

	public String getLastName()
	{
		return _lastName;
	}

	public void setLastName(String lastName)
	{
		this._lastName = lastName;
	}

	

}
