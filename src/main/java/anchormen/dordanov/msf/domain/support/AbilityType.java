package anchormen.dordanov.msf.domain.support;

import anchormen.platform3.domain.support.DatabaseEnum;

/**
 *
 * @author Nico
 */
public enum AbilityType implements DatabaseEnum
{
	BASIC(1, 7),
	SPECIAL(2, 7),
	ULTIMATE(3, 7),
	PASSIVE(4, 5);

	private final Integer _value;
	private final Integer _abilitylLevels;
	private String _languageKey;

	AbilityType(Integer value, Integer abilityLevels)
	{
		_value = value;
		_abilitylLevels = abilityLevels;
		_languageKey = null;
	}

	@Override
	public Integer getValue()
	{
		return _value;
	}

	public Integer getAbilityLevels()
	{
		return _abilitylLevels;
	}

	@Override
	public String getLanguageKey()
	{
		if (_languageKey == null)
		{
			_languageKey = this.getClass().getSimpleName() + "_" + name();
		}
		return _languageKey;
	}
	
}
