package anchormen.dordanov.msf.domain.support;

import anchormen.platform3.domain.support.DatabaseEnum;

/**
 *
 * @author Nico
 */
public enum TraitType implements DatabaseEnum
{
	AFFILIATION(1),
	ORIGIN(2),
	LOCATION(3),
	SIDE(4),
	ROLE(5),
	FLAVOR(6);

	private String _languageKey;
	private final Integer _value;

	TraitType(Integer value)
	{
		_value = value;
		_languageKey = null;
	}

	@Override
	public Integer getValue()
	{
		return _value;
	}

	@Override
	public String getLanguageKey()
	{
		if (_languageKey == null)
		{
			_languageKey = this.getClass().getSimpleName() + "_" + name();
		}
		return _languageKey;
	}
	
}
