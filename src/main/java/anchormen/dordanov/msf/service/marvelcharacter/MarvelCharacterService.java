package anchormen.dordanov.msf.service.marvelcharacter;

import anchormen.dordanov.msf.domain.MarvelCharacter;
import anchormen.platform3.service.EntityCVUDService;

/**
 *
 * @author nico
 */
public interface MarvelCharacterService extends EntityCVUDService<MarvelCharacter> 
{
	
}
