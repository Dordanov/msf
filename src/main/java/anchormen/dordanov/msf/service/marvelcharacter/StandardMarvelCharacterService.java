package anchormen.dordanov.msf.service.marvelcharacter;

import anchormen.dordanov.msf.domain.MarvelCharacter;
import anchormen.platform3.service.AbstractEntityService;
import anchormen.platform3.validation.FeedbackWriter;
import anchormen.platform3.validation.validators.StringMaxLengthValidator;

/**
 *
 * @author n.schouten
 */
public class StandardMarvelCharacterService extends AbstractEntityService<MarvelCharacter> implements MarvelCharacterService
{
	
	public StandardMarvelCharacterService()
	{
		super(MarvelCharacter.class);
	}
		
	@Override
	public boolean validateEntity(MarvelCharacter character, FeedbackWriter feedback)
	{
		boolean valid = true;
				
		valid &= validateProperty(character, feedback, "name", new StringMaxLengthValidator(false, 100));

		return valid;
	}
	
}
