package anchormen.dordanov.msf.service.marvelcharacterabilitylevel;

import anchormen.dordanov.msf.domain.MarvelCharacterAbilityLevel;
import anchormen.platform3.service.EntityCVUDService;

/**
 *
 * @author nico
 */
public interface MarvelCharacterAbilityLevelService extends EntityCVUDService<MarvelCharacterAbilityLevel> 
{
	
}
