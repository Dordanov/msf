package anchormen.dordanov.msf.service.marvelcharacterabilitylevel;

import anchormen.dordanov.msf.domain.MarvelCharacterAbilityLevel;
import anchormen.platform3.service.AbstractEntityService;
import anchormen.platform3.validation.FeedbackWriter;

/**
 *
 * @author n.schouten
 */
public class StandardMarvelCharacterAbilityLevelService extends AbstractEntityService<MarvelCharacterAbilityLevel> implements MarvelCharacterAbilityLevelService
{
	
	public StandardMarvelCharacterAbilityLevelService()
	{
		super(MarvelCharacterAbilityLevel.class);
	}
		
	@Override
	public boolean validateEntity(MarvelCharacterAbilityLevel abilityLevel, FeedbackWriter feedback)
	{
		boolean valid = true;
		return valid;
	}
	
}
