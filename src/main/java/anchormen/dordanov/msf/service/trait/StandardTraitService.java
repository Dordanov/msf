package anchormen.dordanov.msf.service.trait;

import anchormen.dordanov.msf.domain.Trait;
import anchormen.platform3.service.AbstractEntityService;
import anchormen.platform3.validation.FeedbackWriter;
import anchormen.platform3.validation.validators.ObjectNotNullValidator;
import anchormen.platform3.validation.validators.StringMaxLengthValidator;

/**
 *
 * @author n.schouten
 */
public class StandardTraitService extends AbstractEntityService<Trait> implements TraitService
{
	
	public StandardTraitService()
	{
		super(Trait.class);
	}
		
	@Override
	public boolean validateEntity(Trait trait, FeedbackWriter feedback)
	{
		boolean valid = true;
				
		valid &= validateProperty(trait, feedback, "name", new StringMaxLengthValidator(false, 100));
		valid &= validateProperty(trait, feedback, "type", ObjectNotNullValidator.instance());

		return valid;
	}
	
}
