package anchormen.dordanov.msf.service.trait;

import anchormen.dordanov.msf.domain.Trait;
import anchormen.platform3.service.EntityCVUDService;

/**
 *
 * @author nico
 */
public interface TraitService extends EntityCVUDService<Trait> 
{
	
}
