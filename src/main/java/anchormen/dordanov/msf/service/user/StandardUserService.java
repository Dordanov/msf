package anchormen.dordanov.msf.service.user;

import anchormen.dordanov.msf.dao.UserDao;
import anchormen.dordanov.msf.domain.User;
import anchormen.dordanov.msf.domain.search.UserSearchData;
import anchormen.dordanov.msf.validator.MsfPasswordValidator;
import anchormen.java.util.Strings;
import anchormen.platform3.service.AbstractEntityService;
import anchormen.platform3.util.PasswordUtil;
import anchormen.platform3.validation.FeedbackWriter;
import anchormen.platform3.validation.Validator;
import anchormen.platform3.validation.validators.EmailValidator;
import anchormen.platform3.validation.validators.StringMaxLengthValidator;
import java.util.Objects;

/**
 *
 * @author Nico
 */
public class StandardUserService extends AbstractEntityService<User> implements UserService
{
	//private static final Logger LOG = Logger.getLogger(StandardUserService.class.getName());
	
	public StandardUserService()
	{
		super(User.class);
	}
	
	@Override
	public User createEntity()
	{
		User user = super.createEntity();
		user.setSuperUser(Boolean.FALSE);
		return user;
	}
	
	@Override
	public void saveEntity(User e)
	{
		if (e.getPassword() == null)
		{
			// should not be null
			e.setPassword("");
		}
		if (e.getSuperUser() == null)
		{
			// should not be null
			e.setSuperUser(Boolean.FALSE);
		}
		super.saveEntity(e);
	}
	
	@Override
	public boolean validateEntity(User user, FeedbackWriter feedback)
	{
		boolean valid = true;
		
		Validator verplichtMax50 = new StringMaxLengthValidator(false, 50);
		
		valid &= validateProperty(user, feedback, "firstName", verplichtMax50);
		valid &= validateProperty(user, feedback, "lastName", verplichtMax50);
		valid &= validateProperty(user, feedback, "emailAddress", EmailValidator.instance());
				
		if (!Strings.isNullOrEmpty(user.getEmailAddress()))
		{
			UserDao userDao = (UserDao) getDao();
			
			UserSearchData bSD = new UserSearchData();
			bSD.setEmailaddress(user.getEmailAddress());
			
			User existing = userDao.createSelection(bSD).firstOrDefault(null);
			
			if (existing != null && !Objects.equals(existing.getId(), user.getId()))
			{
				feedback.setPropertyMessage("emailAddress", getLocalizedString("emailaddress.already.in.use"));
				valid = false;
			}
		}
		
		return valid;
	}
	
	@Override
	public void setNewPassword(User user, String unencryptedPassword)
	{
		user.setPassword(PasswordUtil.encryptSeeded(unencryptedPassword));
		user.setMustChangePassword(Boolean.FALSE);
		saveEntity(user);
	}
	
}
