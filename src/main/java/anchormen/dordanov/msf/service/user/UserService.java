package anchormen.dordanov.msf.service.user;

import anchormen.dordanov.msf.domain.User;
import anchormen.platform3.service.EntityCVUDService;

/**
 *
 * @author Nico
 */
public interface UserService extends EntityCVUDService<User>
{
	
	public void setNewPassword(User user, String unencryptedPassword);
	
}
