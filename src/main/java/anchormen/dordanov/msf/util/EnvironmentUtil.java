package anchormen.dordanov.msf.util;

import anchormen.dordanov.msf.config.MsfSettings;

/**
 *
 * @author nico
 */
public class EnvironmentUtil
{

	public static boolean isDevelopment()
	{
		return "Development".equals(MsfSettings.getEnvironment());
	}
	
	public static boolean isAcceptatie()
	{
		return "Acceptatie".equals(MsfSettings.getEnvironment());
	}
	
	public static boolean isProduction()
	{
		return "Production".equals(MsfSettings.getEnvironment());
	}
	
}
