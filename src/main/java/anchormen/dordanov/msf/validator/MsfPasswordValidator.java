package anchormen.dordanov.msf.validator;

import anchormen.platform3.validation.FeedbackWriter;
import anchormen.platform3.validation.Validator;
import anchormen.platform3.validation.validators.MultiFieldValidator;
import anchormen.platform3.validation.validators.StringConfirmationPropertyValidator;

/**
 *
 * @author Nico
 */
public class MsfPasswordValidator  extends MultiFieldValidator
{
	private static final MsfPasswordValidator INSTANCE = new MsfPasswordValidator();

	private MsfPasswordValidator()
	{
		super(new StringConfirmationPropertyValidator(6, 500), new MsfPasswordValidatorInternal());
	}

	public static MsfPasswordValidator instance()
	{
		return INSTANCE;
	}
}

class MsfPasswordValidatorInternal implements Validator
{

	@Override
	public boolean validate(Object toValidate, FeedbackWriter feedback)
	{
		return true;
	}
}
