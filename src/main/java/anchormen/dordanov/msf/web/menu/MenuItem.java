/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anchormen.dordanov.msf.web.menu;

import anchormen.dordanov.msf.security.RequiresLogin;
import anchormen.dordanov.msf.web.pages.IndexPage;
import anchormen.dordanov.msf.web.pages.marvelcharacter.MarvelCharacterDetailPage;
import anchormen.dordanov.msf.web.pages.marvelcharacter.MarvelCharacterOverviewPage;
import anchormen.dordanov.msf.web.pages.trait.TraitDetailPage;
import anchormen.dordanov.msf.web.pages.trait.TraitOverviewPage;
import anchormen.dordanov.msf.web.pages.user.UserDetailPage;
import anchormen.dordanov.msf.web.pages.user.UserOverviewPage;
import anchormen.dordanov.msf.web.pages.user.UserProfilePage;
import anchormen.platform3.security.web.Authenticatable;
import anchormen.platform3.web.pages.Page;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author n.schouten
 */
public class MenuItem
{
	private static final Logger LOG = Logger.getLogger(MenuItem.class.getName());
	private static final String SEPARATOR = "SEPARATOR";
	
	private final String _languageKeyPostfix;
	private final Class<? extends Page> _mainPage;
	
	private final Class<? extends Page>[] _catchPages;
	private final List<MenuItem> _subItems = new ArrayList<>();
	
	private Boolean _requiresLogin = false;
	private Boolean _requiresSuperUser = false;	
	private String _action;
	private boolean _newPage = false;
	private boolean _useLink = true;
	private boolean _isSeparator = false;	
	
	
	
	public static List<MenuItem> getMenu()
	{
		List<MenuItem> items = new ArrayList<>();
		
		items.add(new MenuItem("INDEX", IndexPage.class));
		
		MenuItem profileMenu = new MenuItem("MY_PROFILE", null).setUseLink(false).setRequiresLogin(true);
		profileMenu.addSubMenuItem(new MenuItem("USER_PROFILE", UserProfilePage.class));
		/*profileMenu.addSubMenuItem(new MenuItem("USER_RACES", UserRaceTeamOverviewPage.class, UserRaceTeamDetailPage.class));*/
		items.add(profileMenu);
		
		MenuItem analysisMenu = new MenuItem("TEAMS", null).setUseLink(false);
		
		MenuItem managementMenu = new MenuItem("MANAGEMENT", null).setUseLink(false).setRequiresLogin(true);
		managementMenu.addSubMenuItem(new MenuItem("CHARACTERS", MarvelCharacterOverviewPage.class, MarvelCharacterDetailPage.class));
		managementMenu.addSubMenuItem(new MenuItem("TRAITS", TraitOverviewPage.class, TraitDetailPage.class));
		/*managementMenu.addSubMenuItem(new MenuItem(SEPARATOR, null).setIsSeparator(true));		
		managementMenu.addSubMenuItem(new MenuItem(SEPARATOR, null).setIsSeparator(true));
		managementMenu.addSubMenuItem(new MenuItem("TEAMS", TeamOverviewPage.class, TeamDetailPage.class));
		managementMenu.addSubMenuItem(new MenuItem("DRIVERS", DriverOverviewPage.class, DriverDetailPage.class));*/
		managementMenu.addSubMenuItem(new MenuItem(SEPARATOR, null).setIsSeparator(true));
		managementMenu.addSubMenuItem(new MenuItem("USERS", UserOverviewPage.class, UserDetailPage.class));
		/*managementMenu.addSubMenuItem(new MenuItem("JOBS", JobsPage.class));*/
		items.add(managementMenu);
		
		return items;
	}
	
	private MenuItem(String languageKeyPostfix, Class<? extends Page> mainPage, Class<? extends Page>... catchPages)
	{
		_languageKeyPostfix = languageKeyPostfix;
		_mainPage = mainPage;
		_catchPages = catchPages;
		_requiresLogin = mainPage!=null?mainPage.isAnnotationPresent(RequiresLogin.class):false;
	}
	
	private void addSubMenuItem(MenuItem beheerMenuItem)
	{
		_subItems.add(beheerMenuItem);
	}

	
	/* Protected setters met chain opties */
	protected MenuItem setRequiresSuperUser(boolean requiresSuperUser)
	{
		_requiresSuperUser = requiresSuperUser;
		return this;
	}
	protected MenuItem setRequiresLogin(boolean requiresLogin)
	{
		//you can only override this setting if there is no linked page.
		if ( _mainPage == null )
			_requiresLogin = requiresLogin;
		return this;
	}
	protected MenuItem setAction(String action)
	{
		_action = action;
		return this;
	}
	protected MenuItem setNewPage(boolean newPage)
	{
		_newPage = newPage;
		return this;
	}
	
	protected MenuItem setUseLink(boolean useLink)
	{
		_useLink = useLink;
		return this;
	}
	
	protected MenuItem setIsSeparator(boolean isSeparator)
	{
		_isSeparator = isSeparator;
		return this;
	}
	
	
	/* De getters */
	
	public String getLanguageKey()
	{
		return "menuitem_"+_languageKeyPostfix;
	}
	
	public Class<? extends Page> getMainPage()
	{
		return _mainPage;
	}
	
	public String getAction()
	{
		return _action;
	}
	
	public boolean getUseLink()
	{
		return _useLink;
	}
	
	public boolean getNewPage()
	{
		return _newPage;
	}
	
	public boolean getIsSeparator()
	{
		return _isSeparator;
	}
	
	
	public List<MenuItem> getSubItems()
	{
		return _subItems;
	}

	public Class<? extends Page>[] getCatchPages()
	{
		return _catchPages;
	}

	public boolean isActive(Class<? extends Page> page)
	{		
		if ( _mainPage != null && _mainPage.isAssignableFrom(page))
		{
			return true;
		}

		for (Class<? extends Page> catchPage : _catchPages)
		{
			if (catchPage.isAssignableFrom(page) || page.isInstance(catchPage))
			{
				return true;
			}
		}
		
		//als een van de subitems active is dan is het bovenliggende item dat ook!
		
		for ( MenuItem subItem : _subItems )
		{
			if ( subItem.isActive(page) ) //recursieve aanroep. Misschien krijgen we ooit nog wel meer niveaus?
				return true;
		}
		
		return false;
	}

	public boolean isVisible(Authenticatable beheerder)
	{
		Boolean isIngelogd = (beheerder != null);
		Boolean isSuperUser = (beheerder != null);// && beheerder.getSuperUser();

		//de superUser mag altijd overal bij!
		if ( isSuperUser ) 
			return true;
				
		if ( !isIngelogd.equals(_requiresLogin) )
			return false;
		
		if (_requiresSuperUser && !isSuperUser)
			return false;
					
		if ( _isSeparator ) //een seperator die we niet willen verbergen obv showWithAnyOfTheseRights
			return true;
		
		//als de mainPage null is dan is het een seperator of dropdown
		if ( _mainPage == null )
		{
			//bij geen linkje zal het wel een dropdown zijn. Dan hangt het van zijn subitems af. Als je er daar minimaal 1 van mag zien tonen we de super ook.
			if ( !_useLink )
			{
				for ( MenuItem subItem : _subItems )
				{
					if ( !subItem.getIsSeparator() && subItem.isVisible(beheerder) )
						return true;
				}
				return false;
			}
			//useLink staat nog aan, maar we hebben geen page om op te vragen/weer te geven? dit is een config fout.
			LOG.warning("_mainPage is null en _useLink staat aan. Wat verwacht je nu precies van dit menuItem??");
			return false;
		}
		
		//laatste check: zal wel goed zijn..
		return true;
	}
	
	public boolean hasVisibleSubItems(Authenticatable beheerder)
	{
		for ( MenuItem subItem : getSubItems() )
		{
			//seperators do not count!
			if ( !subItem.getIsSeparator() && subItem.isVisible(beheerder) )
					return true;
		}
		//no visible (real) subItems..
		return false;
	}
}
