package anchormen.dordanov.msf.web.pages;

import anchormen.java.lang.reflect.TypeResolver;
import anchormen.platform3.domain.AbstractEntity;
import anchormen.platform3.reflection.fill.FillException;
import anchormen.platform3.service.AbstractEntityService;
import anchormen.platform3.util.StringUtil;
import anchormen.platform3.web.pages.Page;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class AbstractMsfCrudPage<T extends AbstractEntity> extends AbstractMsfPage
{
	private static final Logger LOG = Logger.getLogger(AbstractMsfCrudPage.class.getName());
	
	private final Class<T> _entityClass;
	private T _entity;	
	private AbstractEntityService<T> _entityService;
	
	public AbstractMsfCrudPage()
	{
		super();
		_entityClass = (Class<T>)TypeResolver.erase(TypeResolver.resolveInClass(this.getClass(), AbstractMsfCrudPage.class.getTypeParameters()[0]));
	}
	
	public Page createActionHandler()
	{
		readActionHandler(getEntityService().createEntity());
		return this;
	}
	
	public Page readActionHandler(T entity)
	{
		/*if ( !getEntityService().allowEdit(entity, getAuthenticatable()) )
		{
			addFeedbackError( getLocalizedString("action.edit.not.allowed", getLocalizedString(getBindName())) );
			return getOverviewPage();
		}*/
		setEntity(entity);
		return this;
	}
		
	public Page backActionHandler()
	{
		return getOverviewPage();
	}
		
	
	public Page saveActionHandler() throws FillException
	{
		fillRequiredProperties();
		try
		{
			if ( !fillAndValidateEntity() )
			{
				addFeedbackError( getLocalizedString("action.save.validation.failed", getLocalizedString(getBindName())) );
				return this;
			}

			saveEntity(getEntity());	
			addFeedbackInfo( getLocalizedString("action.save.success", getLocalizedString(getBindName())) );
			
			processOtherUpdates(getEntity());

			return getOverviewPage();
		}
		catch ( Exception e )
		{
			LOG.log(Level.SEVERE, "Exception tijdens saveEntity!", e);
			addFeedbackError( e.getMessage() );
			return this;
		}
	}
	
	protected void saveEntity(T entity)
	{
		getEntityService().saveEntity(entity);
	}
	
	protected void processOtherUpdates(T entity)
	{
	}
		
	protected void fillRequiredProperties() throws FillException
	{
		requiredFill( getRequiredFillProperties() ) ;
	}
	protected boolean fillAndValidateEntity() throws FillException
	{
        //log.info(getBindName());        
		return directSingleFill( getBindName()+".*", getEntityService().entityValidator() );
	}
	
	protected String[] getRequiredFillProperties()
	{
		return new String[]{ getBindName() };
	}
	
	public Page deleteActionHandler(T entity)
	{
		//de entity zetten voor als we hem straks nodig hebben voor een melding of een redirect ofzo (in de getOverviewPage bv)
		try
		{
			setEntity(entity);
			/*if ( !getEntityService().allowDelete(entity, getAuthenticatable()) )
			{
				addFeedbackError( getLocalizedString("action.delete.not.allowed", getLocalizedString(getBindName())) );
				return getOverviewPage();
			}*/
			getEntityService().deleteEntity(entity);
			addFeedbackWarning( getLocalizedString("action.delete.success", getLocalizedString(getBindName())) );
		}
		catch ( Exception e )
		{
			addFeedbackError( e.getMessage() );
		}
		return getOverviewPage();
	}
	
	public abstract Page getOverviewPage();	
	
	public T getEntity()
	{
		return _entity;
	}

	public void setEntity(T entity)
	{
		_entity = entity;
	}
	
	public AbstractEntityService<T> getEntityService()
	{
		if ( _entityService == null )
		{
			_entityService = createEntityService();
		}
		return _entityService;
	}
	
	protected AbstractEntityService createEntityService()
	{
		return getEntityService(_entityClass);
	}
	
	protected String getBindName()
	{
		return StringUtil.getLowerCaseFirst(_entityClass.getSimpleName());
	}

}
