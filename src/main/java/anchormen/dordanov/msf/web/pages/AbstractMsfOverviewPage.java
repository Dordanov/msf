package anchormen.dordanov.msf.web.pages;

import anchormen.java.lang.reflect.TypeResolver;
import anchormen.java.util.Strings;
import anchormen.platform3.dao.searchdata.AbstractSearchDataDao;
import anchormen.platform3.domain.AbstractEntity;
import anchormen.platform3.domain.search.AbstractSearchData;
import anchormen.platform3.reflection.fill.FillException;
import anchormen.platform3.util.CollectionUtil;
import anchormen.platform3.util.StateMapUtil;
import anchormen.platform3.validation.validators.NullValidator;
import anchormen.platform3.validation.validators.ObjectNotNullValidator;
import anchormen.platform3.web.pages.Page;
import anchormen.platform3.web.tags.table.DaoOverviewTableBacking;
import anchormen.platform3.web.tags.table.support.SearchDataQueryHelper;

public abstract class AbstractMsfOverviewPage<T extends AbstractEntity, SD extends AbstractSearchData<T>> extends AbstractMsfPage
{
	private static String SEARCHDATA_SESSION_KEY;
	private static String SORT_COLUMN_SESSION_KEY;
	private static String SORT_DIRECTION_SESSION_KEY;
	
	
	private SearchDataQueryHelper<T, SD> _searchDataQueryHelper;
    private DaoOverviewTableBacking _overview;
	private final Class<T> _entityClass;

	public AbstractMsfOverviewPage()
	{
		_entityClass = (Class<T>) TypeResolver.erase(TypeResolver.resolveInClass(this.getClass(), AbstractMsfOverviewPage.class.getTypeParameters()[0]));
		String simpleClassName = this.getClass().getSimpleName();
		SEARCHDATA_SESSION_KEY = "SearchData_"+simpleClassName;
		SORT_COLUMN_SESSION_KEY = "SortColumn_"+simpleClassName;
		SORT_DIRECTION_SESSION_KEY = "SortDirection_"+simpleClassName;
		constructOverview();
	}

	private void constructOverview()
    {
		_searchDataQueryHelper = createSearchDataQueryHelper();
		_overview = new DaoOverviewTableBacking(_searchDataQueryHelper);
		
		// Het kan zijn dat er (nog) een sortering in de sessie staat.
		// Deze sortering willen we dan meteen instellen.
		String sortColumn = (String)getSessionAttribute(SORT_COLUMN_SESSION_KEY);
		if ( !Strings.isNullOrEmpty(sortColumn) )
			_overview.setSortColumn(sortColumn);
		
		String sortDirection = (String)getSessionAttribute(SORT_DIRECTION_SESSION_KEY);
		if ( !Strings.isNullOrEmpty(sortDirection) )
			_overview.setSortDirection(sortDirection);		
    }
		
	protected SearchDataQueryHelper createSearchDataQueryHelper()
	{
		// Het kan zijn dat er (nog) een SearchData in de sessie staat.
		// Dan gebruiken we die zodat de zoekwaarden behouden blijven.		
		SD cachedSearchData = (SD)getSessionAttribute(SEARCHDATA_SESSION_KEY);
		
		if ( cachedSearchData == null )
		{
			cachedSearchData = createNewSearchData();
			setSessionAttribute(SEARCHDATA_SESSION_KEY, cachedSearchData);
		}
		
		return new SearchDataQueryHelper(cachedSearchData, getSearchDataDao());
	}
	
	protected AbstractSearchDataDao<T, SD>  getSearchDataDao()
	{		 
		 return getDao(_entityClass);
	}
	
	public Page searchActionHandler() throws FillException
	{
		try
		{
			fillSearchData();
		}			
		catch (FillException e)
		{
			//deze situatie willen we negeren.
			if ( !"only a post can be filled".equals(e.getMessage()) )
			{
				throw(e);
			}				
		}

		return this;
	}
	
	protected boolean fillSearchData() throws FillException
	{
		return directSingleFill("searchData.*", ObjectNotNullValidator.instance());
	}

	public Page overviewActionHandler() throws FillException
    {
		//2014-07-25: overviewFill vangt intern al de FilLException af en logt die.
		//dat wil ik niet, dus we doen het nu anders!
        //overviewFill("overview", true);
		try
		{
			directSingleFill("overview.*", NullValidator.instance());
			directSingleFill("state", NullValidator.instance());
			//Sorting in de session opslaan
			setSessionAttribute(SORT_COLUMN_SESSION_KEY, _overview.getSortColumn());
			setSessionAttribute(SORT_DIRECTION_SESSION_KEY, _overview.getSortDirection());
		}			
		catch (FillException e)
		{
			//deze situatie willen we negeren.
			if ( !"only a post can be filled".equals(e.getMessage()) )
			{
				throw(e);
			}
		}

        //constructOverview();
        return this;
    }
	
	protected SD createNewSearchData()
	{
		return getSearchDataDao().createSearchData();	
	}
	
	protected SearchDataQueryHelper<T, SD> getSearchDataQueryHelper()
	{
		return _searchDataQueryHelper;
	}

    public DaoOverviewTableBacking getOverview()
    {
        return _overview;
    }

	public SD getSearchData()
	{
		return _searchDataQueryHelper.getSearchData();
	}
	
	public void setSearchData(SD sd)
	{
		_searchDataQueryHelper.setSearchData(sd);
	}	
	
	public void setState(String stateString)
	{
		StateMapUtil.setPropertiesState(stateString, this, "searchData");
	}

	public String getState()
	{
		return StateMapUtil.getPropertiesState(this, "searchData");
	}	
	
	public <T> T getFirstOrNull(Iterable<? extends T> c)
	{
		return CollectionUtil.getFirstOrNull(c);
	}
}
