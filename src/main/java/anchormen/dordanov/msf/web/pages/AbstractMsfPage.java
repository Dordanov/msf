package anchormen.dordanov.msf.web.pages;

import anchormen.dordanov.msf.domain.User;
import anchormen.dordanov.msf.security.RequiresLogin;
import anchormen.dordanov.msf.web.pages.user.ChangePasswordPage;
import anchormen.platform3.service.AbstractEntityService;
import anchormen.platform3.util.StringUtil;
import anchormen.platform3.web.pages.MasteredPage;
import anchormen.platform3.web.pages.Page;
import anchormen.platform3.web.pages.security.AbstractAuthenticationRequiredPage;

/**
 *
 * @author nico
 *
 */
public abstract class AbstractMsfPage extends AbstractAuthenticationRequiredPage<User> implements MasteredPage
{

	public AbstractMsfPage()
	{
		super("user", LoginPage.class);
	}

	@Override
	protected final Page onAuthenticatedPageUse()
	{

		//If a user is logged in and must change the password: redirect to the ChangePasswordPage.
		if (getAuthenticatable() != null && Boolean.TRUE.equals(getAuthenticatable().getMustChangePassword()))
		{
			//Don't redirect (again) if we are already there.
			if (!getClass().equals(ChangePasswordPage.class) && !getClass().equals(LogoutPage.class))
			{
				addFeedbackWarning(getLocalizedString("user.must.change.password"));
				return clientRedirectPage(ChangePasswordPage.class);
			}
		}

		if ( getAuthenticatable() != null )
		{
		}

		return onMsfPageUse();
	}
	
	protected Page onMsfPageUse()
	{
		return this;
	}
	
	@Override
	protected boolean isAuthenticated()
	{
		//Does this page require you to login?
		if ( this.getClass().isAnnotationPresent(RequiresLogin.class) )
		{
			//In that case check if we are indeed logged in.
			return getAuthenticatable() != null;
		}

		return true;
	}

	@Override
	protected boolean isValidAuthenticatable(User authenticatable)
	{
		//We can do a check on rights here if we are going to use them.

		//return authenticatable != null && authenticatable.getId() != null;
		return true;
	}

	public AbstractEntityService getEntityService(Class entityClass)
	{
		return (AbstractEntityService) getBean(StringUtil.getLowerCaseFirst(entityClass.getSimpleName()) + "Service");
	}

	protected void setSessionAttribute(String sessionKey, Object objToStore)
	{
		getRequest().getSession().setAttribute(sessionKey, objToStore);
	}

	protected Object getSessionAttribute(String sessionKey)
	{
		return getRequest().getSession().getAttribute(sessionKey);
	}

	@Override
	public String getTitle()
	{
		return getLocalizedPageString("title");
	}

	@Override
	public boolean isStateful()
	{
		return false;
	}

	@Override
	public String jspMasterPage()
	{
		return "/WEB-INF/resources/master-pages/masterPage.jsp";
	}

}
