package anchormen.dordanov.msf.web.pages;

import anchormen.platform3.web.pages.MasteredPage;
import anchormen.platform3.web.pages.security.IgnorePageAuthenticationRequirement;

/**
 *
 * @author nico
 */
@IgnorePageAuthenticationRequirement
public class IndexPage extends AbstractMsfPage implements MasteredPage
{
	
}
