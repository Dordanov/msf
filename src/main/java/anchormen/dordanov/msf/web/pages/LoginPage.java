package anchormen.dordanov.msf.web.pages;

import anchormen.dordanov.msf.dao.UserDao;
import anchormen.dordanov.msf.domain.User;
import anchormen.dordanov.msf.domain.search.UserSearchData;
import anchormen.java.util.Strings;
import anchormen.platform3.util.PasswordUtil;
import anchormen.platform3.validation.Validator;
import anchormen.platform3.validation.validators.EmailValidator;
import anchormen.platform3.validation.validators.NullValidator;
import anchormen.platform3.validation.validators.StringMaxLengthValidator;
import anchormen.platform3.web.pages.Page;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Nico
 */
public class LoginPage extends AbstractMsfPage
{

	private String _emailAddress;
	private String _password;

	
	public Page loginActionHandler()
	{
		boolean loopbackAddress;
		try
		{
			InetAddress address = InetAddress.getByName(getRequest().getRemoteAddr());
			loopbackAddress = address.isLoopbackAddress();
		}
		catch (UnknownHostException e)
		{
			loopbackAddress = false;
		}
		
		Map<String, Validator> validators = new HashMap<>();

		if (loopbackAddress)
		{
			validators.put("emailAddress", NullValidator.instance());
			validators.put("password", NullValidator.instance());
		}
		else
		{
			validators.put("emailAddress", EmailValidator.instance());
			validators.put("password", new StringMaxLengthValidator(false, 50));
		}

		if ( directFill(validators) )
		{
			UserDao dao = getDao(User.class);
			User user;
			if ( loopbackAddress && Strings.isNullOrEmpty(getEmailAddress()) && Strings.isNullOrEmpty(getPassword()) )
			{
				user = dao.findById(1);
				addFeedbackInfo("Nico-O-Matic Localhost Auto-Login™ Enabled");
			}
			else
			{
				UserSearchData bSD = new UserSearchData();
				bSD.setEmailaddress(getEmailAddress());
				bSD.setPasswordEncrypted(PasswordUtil.encryptSeeded(getPassword()));
				user = dao.createSelection(bSD).firstOrDefault(null);
			}

			if ( user != null && !user.isNew() )
			{					
				setAuthenticatable(user);
				addFeedbackInfo( getLocalizedString("login.success", user.getFirstName()) );
				return clientRedirectPage(IndexPage.class);
			}
		}
		
		addFeedbackError(getLocalizedString("login.error"));
		return this;
	}
	
	public Page logoutActionHandler()
	{
		setAuthenticatable(null);
		addFeedbackInfo( getLocalizedString("logout.success") );
		return clientRedirectPage(IndexPage.class);
	}
	
	public String getEmailAddress()
	{
		return _emailAddress;
	}

	public void setEmailAddress(String emailadres)
	{
		_emailAddress = emailadres;
	}

	public String getPassword()
	{
		return _password;
	}

	public void setPassword(String wachtwoord)
	{
		_password = wachtwoord;
	}
}
