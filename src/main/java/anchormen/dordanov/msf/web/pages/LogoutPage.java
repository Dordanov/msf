package anchormen.dordanov.msf.web.pages;

import anchormen.dordanov.msf.security.RequiresLogin;

/**
 *
 * @author Nico
 */
@RequiresLogin
public class LogoutPage extends LoginPage
{
	
}
