package anchormen.dordanov.msf.web.pages.marvelcharacter;

import anchormen.dordanov.msf.dao.TraitDao;
import anchormen.dordanov.msf.domain.MarvelCharacter;
import anchormen.dordanov.msf.domain.Trait;
import anchormen.dordanov.msf.domain.search.TraitSearchData;
import anchormen.dordanov.msf.domain.support.TraitType;
import anchormen.dordanov.msf.security.RequiresLogin;
import anchormen.dordanov.msf.web.pages.AbstractMsfCrudPage;
import anchormen.platform3.web.pages.Page;
import java.util.List;

/**
 *
 * @author Nico
 */
@RequiresLogin
public class MarvelCharacterDetailPage extends AbstractMsfCrudPage<MarvelCharacter>
{	
	private TraitDao _traitDao;

	@Override
	protected Page onMsfPageUse()
	{
		_traitDao = getDao(Trait.class);
		return this;
	}
	
	public Page addTraitActionHandler(MarvelCharacter character, Trait trait)
	{
		character.getTraits().add(trait);
		saveEntity(character);		
		return readActionHandler(character);
	}
	
	public Page removeTraitActionHandler(MarvelCharacter character, Trait trait)
	{
		character.getTraits().remove(trait);
		saveEntity(character);		
		return readActionHandler(character);
	}

	
	public MarvelCharacter getMarvelCharacter()
	{
		return getEntity();
	}
	
	public void setMarvelCharacter(MarvelCharacter entity)
	{
		setEntity(entity);
	}

	public List<Trait> getTraitOptions(TraitType traitType)
	{
		TraitSearchData tsd = _traitDao.createSearchData();
		tsd.setTraitType(traitType);
		List<Trait> options = _traitDao.findBySearchData(tsd);
		options.removeAll(getMarvelCharacter().getTraits());
		return options;
	}
	
	@Override
	public Page getOverviewPage()
	{
		return clientRedirectPage(MarvelCharacterOverviewPage.class);
	}
	
}
