/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anchormen.dordanov.msf.web.pages.marvelcharacter;

import anchormen.dordanov.msf.domain.MarvelCharacter;
import anchormen.dordanov.msf.domain.search.MarvelCharacterSearchData;
import anchormen.dordanov.msf.security.RequiresLogin;
import anchormen.dordanov.msf.web.pages.AbstractMsfOverviewPage;
import anchormen.platform3.web.pages.Page;

/**
 *
 * @author Nico
 */
@RequiresLogin
public class MarvelCharacterOverviewPage extends AbstractMsfOverviewPage<MarvelCharacter, MarvelCharacterSearchData>
{
	//private static final Logger LOG = Logger.getLogger(MarvelCharacterOverviewPage.class.getName());

	@Override
	protected Page onMsfPageUse() 
	{
		getOverview().setResultsPerPageOptions(new int[]{0});
		getOverview().setResultsPerPage(0);
		return this;
	}
	
}
