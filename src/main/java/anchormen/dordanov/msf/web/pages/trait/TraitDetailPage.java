package anchormen.dordanov.msf.web.pages.trait;

import anchormen.dordanov.msf.domain.Trait;
import anchormen.dordanov.msf.security.RequiresLogin;
import anchormen.dordanov.msf.web.pages.AbstractMsfCrudPage;
import anchormen.platform3.web.pages.Page;

/**
 *
 * @author Nico
 */
@RequiresLogin
public class TraitDetailPage extends AbstractMsfCrudPage<Trait>
{
	
	
	
	public Trait getTrait()
	{
		return getEntity();
	}
	
	public void setTrait(Trait entity)
	{
		setEntity(entity);
	}

	@Override
	public Page getOverviewPage()
	{
		return clientRedirectPage(TraitOverviewPage.class);
	}
	
}
