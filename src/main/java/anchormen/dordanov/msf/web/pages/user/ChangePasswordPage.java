package anchormen.dordanov.msf.web.pages.user;

import anchormen.dordanov.msf.security.RequiresLogin;
import anchormen.dordanov.msf.service.user.UserService;
import anchormen.dordanov.msf.validator.MsfPasswordValidator;
import anchormen.dordanov.msf.web.pages.AbstractMsfPage;
import anchormen.dordanov.msf.web.pages.IndexPage;
import anchormen.platform3.converter.factory.ConverterFactory;
import anchormen.platform3.localization.LocalizedStringProvider;
import anchormen.platform3.util.PasswordUtil;
import anchormen.platform3.validation.Validator;
import anchormen.platform3.validation.validators.StringValidator;
import anchormen.platform3.web.pages.Page;
import anchormen.platform3.web.pages.feedback.PageFeedback;
import anchormen.platform3.web.support.StringConfirmationProperty;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Nico
 */
@RequiresLogin
public class ChangePasswordPage extends AbstractMsfPage
{
	private String _oldPassword;
    private StringConfirmationProperty _password = new StringConfirmationProperty();
	
		
	public Page changeActionHandler()
    {
		Map<String, Validator> validators = new HashMap<>();
		if ( !Boolean.TRUE.equals(getAuthenticatable().getMustChangePassword()) )
		{
			validators.put("oldPassword", new StringValidator()
			{

				@Override
				public String isValid(Object toValidate, LocalizedStringProvider lsp, ConverterFactory converterFactory)
				{
					if ( !(PasswordUtil.encryptSeeded((String) toValidate).equals(getAuthenticatable().getPassword())) )
					{
						return lsp.getString("validation.invalid.password");
					}
					return null;
				}

			});
		}
		validators.put("password.*", MsfPasswordValidator.instance());
		if ( directFill(validators) )
		{
			UserService service = getService(UserService.class);
			service.setNewPassword(getAuthenticatable(), getPassword().getValue());
			PageFeedback.getFeedback().addInfo(getLocalizedString("password.changed"));
			return newPage(IndexPage.class);
		}

        return this;
    }

	public String getOldPassword()
	{
		return _oldPassword;
	}

	public void setOldPassword(String password)
	{
		_oldPassword = password;
	}

	public StringConfirmationProperty getPassword()
	{
		return _password;
	}

	public void setWachtwoord(StringConfirmationProperty password)
	{
		_password = password;
	}
	
	
}
