package anchormen.dordanov.msf.web.pages.user;

import anchormen.dordanov.msf.domain.User;
import anchormen.dordanov.msf.security.RequiresLogin;
import anchormen.dordanov.msf.service.user.UserService;
import anchormen.dordanov.msf.validator.MsfPasswordValidator;
import anchormen.dordanov.msf.web.pages.AbstractMsfCrudPage;
import anchormen.java.util.Strings;
import anchormen.platform3.reflection.fill.FillException;
import anchormen.platform3.validation.validators.NullValidator;
import anchormen.platform3.web.pages.Page;
import anchormen.platform3.web.support.StringConfirmationProperty;

/**
 *
 * @author Nico
 */
@RequiresLogin
public class UserDetailPage extends AbstractMsfCrudPage<User>
{
	private StringConfirmationProperty _newPassword = new StringConfirmationProperty();
	
	
	@Override
	protected boolean fillAndValidateEntity() throws FillException
	{		
		boolean valid = super.fillAndValidateEntity();
		
		//do a nullFill first so we can see if it is in the request without validation errors.
		directSingleFill("newPassword.value", NullValidator.instance());
				
		//not sending an email for now, so just supply a correct (new) password for new users or when you enter something
		if ( getUser().isNew() || !Strings.isNullOrEmpty(_newPassword.getValue()) )
		{
			valid &= directSingleFill("newPassword.*", MsfPasswordValidator.instance());
		}
		
		if ( valid )
		{		
			//bij een nieuwe beheerder moet er een tijdelijk wachtwoord gemailed worden
			if ( getUser().isNew() )
			{
				getUser().setSuperUser(Boolean.FALSE);
				
				//nieuwe wachtwoord zetten voor de beheerder
				UserService service = getService(UserService.class);
				if ( !Strings.isNullOrEmpty(_newPassword.getValue()) )
				{
					service.setNewPassword(getUser(), _newPassword.getValue());
					getUser().setMustChangePassword(Boolean.TRUE);					
				}
			}
		}
		
		return valid;
	}
	
	
	public User getUser()
	{
		return getEntity();
	}

	public void setUser(User user)
	{
		setEntity(user);
	}

	public StringConfirmationProperty getNewPassword()
	{
		return _newPassword;
	}

	public void setNewPassword(StringConfirmationProperty newPassword)
	{
		_newPassword = newPassword;
	}
	
	
	@Override
	public Page getOverviewPage()
	{
		return clientRedirectPage(UserOverviewPage.class);
	}
}
