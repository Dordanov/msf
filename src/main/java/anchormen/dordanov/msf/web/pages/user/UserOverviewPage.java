package anchormen.dordanov.msf.web.pages.user;

import anchormen.dordanov.msf.domain.User;
import anchormen.dordanov.msf.domain.search.UserSearchData;
import anchormen.dordanov.msf.security.RequiresLogin;
import anchormen.dordanov.msf.web.pages.AbstractMsfOverviewPage;
import anchormen.java.lang.Predicate;
import anchormen.platform3.web.pages.Page;

/**
 *
 * @author Nico
 */
@RequiresLogin
public class UserOverviewPage extends AbstractMsfOverviewPage<User, UserSearchData>
{

	public Page loginAsActionHandler(User user)
	{
		getRequest().getSession().invalidate();
		setAuthenticatable(user);
		addFeedbackInfo("You are now logged in as " + user.getName());
		return this;
	}

	public Predicate showLoginAs()
	{
		return new Predicate<Object>()
		{
			@Override
			public boolean evaluate(Object e)
			{
				//for now allow everyone to use this (convenience for switching)
				return true;
				//return Boolean.TRUE.equals(getAuthenticatable().getSuperUser());
			}
		};
	}

	public Predicate allowDelete()
	{
		return new Predicate<Object>()
		{
			@Override
			public boolean evaluate(Object e)
			{
				return !getAuthenticatable().equals(e);
			}
		};
	}
	
}
