package anchormen.dordanov.msf.web.pages.user;

import anchormen.dordanov.msf.domain.User;
import anchormen.dordanov.msf.security.RequiresLogin;
import anchormen.dordanov.msf.service.user.UserService;
import anchormen.dordanov.msf.web.pages.AbstractMsfPage;
import anchormen.platform3.web.pages.Page;

/**
 *
 * @author Nico
 */
@RequiresLogin
public class UserProfilePage extends AbstractMsfPage
{
	private User _user;

	@Override
	protected Page onMsfPageUse()
	{
		_user = getDao(User.class).findById(getAuthenticatable().getId());
		return this;
	}
	
	public Page saveActionHandler()
	{
		UserService userService = getService(UserService.class);
		
		if ( !directSingleFill("user.*", userService.entityValidator()) )
		{
			addFeedbackError( getLocalizedString("action.save.validation.failed", "Profile") );
			return this;
		}
	
		userService.saveEntity(_user);
		//update the authenticatable as well.
		setAuthenticatable(_user);
		addFeedbackInfo( getLocalizedString("action.save.success", "Profile") );
		
		return this;
	}

	
	public User getUser()
	{
		return _user;
	}

	public void setUser(User user)
	{
		_user = user;
	}	
}
