<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" extends="anchormen.platform3.web.servlet.JspSpringServlet"
%><%@page import="anchormen.dordanov.msf.web.pages.LoginPage"
%><%@taglib uri="/am" prefix="am"
%><%
	LoginPage p = getCurrentPage();
%>

<div class="row">
	<div class="col col-md-2"></div>
	<div class="col col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading"><h4><%= getString("login") %></h4></div>
			<am:page-form action="login" tableClass="table table-striped">
				<am:input-text boundpath="emailAddress" inputClass="form-control" />
				<am:input-text boundpath="password" inputClass="form-control" password="true" />
				<am:page-form-submit value="login" buttonClass="btn btn-primary" trClass="submit-button-row" />
			</am:page-form>
		</div>
	</div>
	<div class="col col-md-2"></div>
</div>
