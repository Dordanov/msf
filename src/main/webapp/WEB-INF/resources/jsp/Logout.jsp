<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" extends="anchormen.platform3.web.servlet.JspSpringServlet"
%><%@page import="anchormen.dordanov.msf.web.pages.LoginPage"
%><%@taglib uri="/am" prefix="am"
%><%
	LoginPage p = getCurrentPage();
%>

<div class="row">
	<div class="col col-md-2"></div>
	<div class="col col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading"><h4><%= getString("logout") %></h4></div>
			<am:page-form action="logout" tableClass="table table-striped">
				<tr>
					<td></td>
					<td><%= getString("user.logged.in.as", p.getAuthenticatable().getName()) %></td>
				</tr>
				<am:page-form-submit value="logout" buttonClass="btn btn-primary" trClass="submit-button-row" />
			</am:page-form>
		</div>
	</div>
	<div class="col col-md-2"></div>
</div>
