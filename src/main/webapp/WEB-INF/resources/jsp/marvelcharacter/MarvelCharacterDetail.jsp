<%@page import="anchormen.dordanov.msf.domain.Trait"%>
<%@page import="anchormen.dordanov.msf.domain.support.TraitType"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" extends="anchormen.platform3.web.servlet.JspSpringServlet"
%><%@page import="anchormen.dordanov.msf.domain.support.AbilityType"
%><%@page import="anchormen.dordanov.msf.web.pages.marvelcharacter.*"
%><%@taglib uri="/am" prefix="am"
%><%
	MarvelCharacterDetailPage p = getCurrentPage();
%>
<div class="row">
	<div class="col col-md-12">
		
		<div class="back-nav">
			<am:page-link action="back" cssClass="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span> <am:localized-text key="back.to.overview" /></am:page-link>
		</div>

		<am:page-form action="save" renderTable="false"> 

			<am:input-hidden boundpath="marvelCharacter" />

			<table class="table">
				<am:input-text boundpath="marvelCharacter.name" inputClass="form-control" />
				<am:input-text boundpath="marvelCharacter.level" inputClass="form-control" />
				<am:input-text boundpath="marvelCharacter.starRank" inputClass="form-control" />
				<am:input-text boundpath="marvelCharacter.redStarRank" inputClass="form-control" />
				<am:input-text boundpath="marvelCharacter.gearTier" inputClass="form-control" />
				<am:input-text boundpath="marvelCharacter.power" inputClass="form-control" />
				<am:page-form-submit value="save" buttonClass="btn btn-primary" trClass="button-row" />
			</table>

		</am:page-form>
	</div>
</div>
<div class="row">
	<div class="col col-md-12">
		<h3><%= getString("charactertraits") %></h3>
		<ul>
			<% for ( Trait trait : p.getMarvelCharacter().getTraits() )  { %>
				<li>
					<%= trait.getName() %>
					<am:page-link cssClass="badge badge-danger" action="removeTrait" arguments="<%= new Object[]{p.getMarvelCharacter(), trait} %>"><span class="glyphicon glyphicon-minus"></span></am:page-link>
				</li>
			<% } %>
		</ul>
	</div>
	<% for ( TraitType traitType : TraitType.values() ) { %>
		<div class="col col-md-<%= 12/TraitType.values().length %>">
			<%= toLabel(TraitType.class, traitType) %>
			<ul>
				<% for ( Trait trait : p.getTraitOptions(traitType) ) { %>
				<li>
					<%= trait.getName() %>
					<am:page-link cssClass="badge badge-success" action="addTrait" arguments="<%= new Object[]{p.getMarvelCharacter(), trait} %>">
						<span class="glyphicon glyphicon-plus"></span>
					</am:page-link>
				</li>
				<% } %>
			</ul>
		</div>
	<% } %>
</div>
		
<div class="row">
	<% for ( AbilityType abilityType : AbilityType.values() ) { %>
		<div class="col col-md-3">
			<h3><%= toLabel(AbilityType.class, abilityType) %></h3>
		</div>
	<% } %>
</div>