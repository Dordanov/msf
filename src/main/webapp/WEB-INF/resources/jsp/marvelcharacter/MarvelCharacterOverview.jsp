<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" extends="anchormen.platform3.web.servlet.JspSpringServlet"
%><%@page import="anchormen.dordanov.msf.domain.MarvelCharacter"
%><%@page import="anchormen.dordanov.msf.web.pages.marvelcharacter.*"
%><%@taglib uri="/am" prefix="am"
%><%
	MarvelCharacterOverviewPage p = getCurrentPage();
%>
<div class="row">
	<div class="col col-md-12">
		<am:page-link action="create" cssClass="btn btn-primary pull-right" pageclass="<%= MarvelCharacterDetailPage.class  %>"><am:localized-text key="add.marvelcharacter"/></am:page-link>
		
		<am:overviewtable boundpath="overview">
			<table class="table table-striped">
				<am:overviewtable-header>
					<am:overviewtable-header-column boundpath="name" />
					<am:overviewtable-header-column boundpath="level" />
					<am:overviewtable-header-column boundpath="starRank" />
					<am:overviewtable-header-column boundpath="redStarRank" />
					<am:overviewtable-header-column boundpath="gearTier" />
					<am:overviewtable-header-column label="" />
				</am:overviewtable-header>
				<am:overviewtable-body var="character">
					<% MarvelCharacter character = (MarvelCharacter) pageContext.getAttribute("character"); %>
					<am:overviewtable-body-text-column boundpath="name" />
					<am:overviewtable-body-text-column boundpath="level" />
					<am:overviewtable-body-text-column boundpath="starRank" />
					<am:overviewtable-body-text-column boundpath="redStarRank" />
					<am:overviewtable-body-text-column boundpath="gearTier" />
					<am:overviewtable-body-action-column action="read" pageClass="<%= MarvelCharacterDetailPage.class %>" linkClass="glyphicon glyphicon-pencil" title="edit" />
				</am:overviewtable-body>
			</table>
			<am:overviewtable-pager />
		</am:overviewtable>
	</div>
</div>