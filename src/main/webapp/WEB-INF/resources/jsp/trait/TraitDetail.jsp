<%@page import="anchormen.dordanov.msf.domain.support.TraitType"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" extends="anchormen.platform3.web.servlet.JspSpringServlet"
%><%@page import="anchormen.dordanov.msf.web.pages.trait.*"
%><%@taglib uri="/am" prefix="am"
%><%
	TraitDetailPage p = getCurrentPage();
%>
<div class="row">
	<div class="col col-md-12">
		
		<div class="back-nav">
			<am:page-link action="back" cssClass="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span> <am:localized-text key="back.to.overview" /></am:page-link>
		</div>

		<am:page-form action="save" renderTable="false"> 
			<am:input-hidden boundpath="trait" />
			<table class="table">
				<am:input-text boundpath="trait.name" inputClass="form-control" />
				<am:input-combobox boundpath="trait.type" inputClass="form-control" options="<%= TraitType.values() %>" includeEmpty="true" optionValueProperty="" optionViewProperty="" />
				<am:page-form-submit value="save" buttonClass="btn btn-primary" trClass="button-row" />
			</table>

		</am:page-form>
	</div>
</div>