<%@page import="anchormen.dordanov.msf.domain.support.TraitType"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" extends="anchormen.platform3.web.servlet.JspSpringServlet"
%><%@page import="anchormen.dordanov.msf.domain.Trait"
%><%@page import="anchormen.dordanov.msf.web.pages.trait.*"
%><%@taglib uri="/am" prefix="am"
%><%
	TraitOverviewPage p = getCurrentPage();
%>
<div class="row">
	<div class="col col-md-12">
		
		<am:page-form action="search" renderTable="false">
			<table class="table table-striped">
				<am:input-combobox boundpath="searchData.traitType" options="<%= TraitType.values() %>" includeEmpty="true" optionValueProperty="" optionViewProperty="" />
				<am:page-form-submit value="search" />
			</table>			
		</am:page-form>
		
		
		<am:page-link action="create" cssClass="btn btn-primary pull-right" pageclass="<%= TraitDetailPage.class  %>"><am:localized-text key="add.trait"/></am:page-link>
		
		<am:overviewtable boundpath="overview">
			<table class="table table-striped">
				<am:overviewtable-header>
					<am:overviewtable-header-column boundpath="name" />
					<am:overviewtable-header-column boundpath="type" />
					<am:overviewtable-header-column label="" />
				</am:overviewtable-header>
				<am:overviewtable-body var="trait">
					<am:overviewtable-body-text-column boundpath="name" />
					<am:overviewtable-body-text-column boundpath="type" />
					<am:overviewtable-body-action-column action="read" pageClass="<%= TraitDetailPage.class %>" linkClass="glyphicon glyphicon-pencil" title="edit" />
				</am:overviewtable-body>
			</table>
			<am:overviewtable-pager />
		</am:overviewtable>
	</div>
</div>