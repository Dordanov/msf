<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" extends="anchormen.platform3.web.servlet.JspSpringServlet"
%><%@page import="anchormen.dordanov.msf.web.pages.user.ChangePasswordPage"
%><%@taglib uri="/am" prefix="am"
%><%
	ChangePasswordPage p = getCurrentPage();
%>

<div class="row">
	<div class="col col-md-6">
		<am:page-form action="change" tableClass="table table-striped">
			<% if ( !Boolean.TRUE.equals(p.getAuthenticatable().getMustChangePassword()) ) { %>
				 <am:input-text boundpath="oldPassword" password="true" inputClass="form-control" />     
			<% } %>
			<am:input-text boundpath="password.value" label="password" password="true" inputClass="form-control" />
			<am:input-text boundpath="password.confirmation" label="passwordAgain" password="true" inputClass="form-control" />
			<am:page-form-submit value="save" buttonClass="btn btn-primary" trClass="submit-button-row" />
		</am:page-form>
	</div>
</div>