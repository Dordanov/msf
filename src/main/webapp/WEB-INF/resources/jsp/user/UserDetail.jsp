<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" extends="anchormen.platform3.web.servlet.JspSpringServlet"
%><%@page import="anchormen.dordanov.msf.web.pages.user.UserDetailPage"
%><%@taglib uri="/am" prefix="am"
%><%
	UserDetailPage p = getCurrentPage();
%>

<div class="row">
		<div class="col col-md-6">
			<am:page-form action="save" renderTable="false"> 
				<am:input-hidden boundpath="user" />
				<table class="table table-striped">
					<am:input-text boundpath="user.firstName" inputClass="form-control" />
					<am:input-text boundpath="user.lastName" inputClass="form-control" />
					<am:input-text boundpath="user.emailAddress" inputClass="form-control" />
					<% if ( !p.getUser().isNew() ) { %>
						<tr>
							<th></th>
							<td>Only supply the password if you want to change it.</td>
						</tr>
					<% } %>
					<am:input-text boundpath="newPassword.value" inputClass="form-control" label="password" password="true" />
					<am:input-text boundpath="newPassword.confirmation" inputClass="form-control" label="passwordAgain" password="true" />


					<am:page-form-submit value="save" buttonClass="btn btn-primary" trClass="submit-button-row" />
				</table>
			</am:page-form>
	</div>
</div>