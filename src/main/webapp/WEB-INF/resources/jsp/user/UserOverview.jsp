<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" extends="anchormen.platform3.web.servlet.JspSpringServlet"
%><%@page import="anchormen.dordanov.msf.web.pages.user.*"
%><%@taglib uri="/am" prefix="am"
%><%
	UserOverviewPage p = getCurrentPage();
%>

<div class="row">
	<am:page-form action="zoeken" renderTable="false">
		<div class="col-xs-12 col-sm-6">
			<div class="panel panel-default">
				<table class="table table-striped">
					<am:input-text boundpath="searchData.firstName" inputClass="form-control" />				
					<am:input-text boundpath="searchData.lastName" inputClass="form-control" />
					<am:page-form-submit value="search" buttonClass="btn btn-primary" trClass="submit-button-row" />
				</table>
			</div>
		</div>
	</am:page-form>
</div>

<div class="row">
	<div class="col col-md-12">
		<am:page-link action="create" cssClass="btn btn-primary pull-right" pageclass="<%= UserDetailPage.class  %>"><span class="glyphicon glyphicon-plus"></span> <am:localized-text key="add.user"/></am:page-link>

		<am:overviewtable boundpath="overview">
			<table class="table table-striped">
				<am:overviewtable-header>
					<am:overviewtable-header-column boundpath="firstName" />
					<am:overviewtable-header-column boundpath="lastName" />
					<am:overviewtable-header-column boundpath="emailAddress" />
					<am:overviewtable-header-column label="" />
					<am:overviewtable-header-column label="" />
					<am:overviewtable-header-column label="" />
				</am:overviewtable-header>
				<am:overviewtable-body var="b">
					<am:overviewtable-body-text-column boundpath="firstName" />
					<am:overviewtable-body-text-column boundpath="lastName" />
					<am:overviewtable-body-text-column boundpath="emailAddress" />			
					<am:overviewtable-body-action-column action="read" linkClass="glyphicon glyphicon-pencil" pageClass="<%= UserDetailPage.class %>" title="edit" />
					<am:overviewtable-body-action-column action="loginAs" linkClass="glyphicon glyphicon-user" pageClass="<%= p.getClass() %>" title="login.as.user" showAction="<%= p.showLoginAs() %>" />
					<am:overviewtable-body-confirm-action-column action="delete" linkClass="glyphicon glyphicon-remove" pageClass="<%= UserDetailPage.class %>" confirmText="confirm.delete" showAction="<%= p.allowDelete() %>"/>
				</am:overviewtable-body>
			</table>
			<am:overviewtable-pager />
		</am:overviewtable>
	</div>
</div>