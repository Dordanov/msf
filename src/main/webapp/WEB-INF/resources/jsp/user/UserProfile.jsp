<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" extends="anchormen.platform3.web.servlet.JspSpringServlet"
%><%@page import="anchormen.dordanov.msf.web.pages.user.*"
%><%@taglib uri="/am" prefix="am"
%><%
	UserProfilePage p = getCurrentPage();
%>

<div class="row">
	<div class="col col-md-6">
		<am:page-form action="save" renderTable="false"> 
			<am:input-hidden boundpath="user" />
			<table class="table table-striped">
				<am:input-text boundpath="user.firstName" inputClass="form-control" />
				<am:input-text boundpath="user.lastName" inputClass="form-control" />
				<am:input-text boundpath="user.emailAddress" inputClass="form-control" />
				<am:page-form-submit value="save" buttonClass="btn btn-primary" trClass="submit-button-row" />
			</table>
		</am:page-form>
	</div>
</div>