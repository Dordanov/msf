<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" extends="anchormen.platform3.web.servlet.JspSpringServlet"	
%><%@page import="anchormen.platform3.web.pages.AbstractBasePage"
%><%@page import="anchormen.java.util.Strings"
%><%@taglib uri="/am" prefix="am"
%><%
	AbstractBasePage p = getCurrentPage();
%><!DOCTYPE html><!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="imagetoolbar" content="no">
		<meta name="robots" content="noindex,nofollow">
        <!-- made by Dordanov(c) 2018 -->
        <title>MSF Tool</title>
		<base href="<%= getServletBaseUri(request)%>" />
		<link rel="stylesheet" href="styles/bootstrap.min.css?ts=20181207230000" />
		<link rel="stylesheet" href="styles/bootstrap-theme.min.css?ts=20181207230000" />
		<link rel="stylesheet" href="styles/develop.css" />
    </head>
    <body>
		<jsp:include page="../menu/Menu.jsp"></jsp:include>
		<div class="container">
			<div class="row">
				<div class="col col-md-12">
					<h1><%= p.getTitle() %></h1>
				</div>
			</div>
			<am:page-feedback />
			
			<jsp:include page="${ContentPage}"></jsp:include>
		</div>
		<div class="footer">
		</div>
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="js/jquery-3.2.1.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.min.js"></script>
    </body>
</html>