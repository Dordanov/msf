<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" extends="anchormen.platform3.web.servlet.JspSpringServlet"
%><%@page import="anchormen.dordanov.msf.web.menu.MenuItem"
%><%@page import="anchormen.dordanov.msf.web.pages.*"
%><%@page import="anchormen.dordanov.msf.web.pages.user.UserProfilePage"
%><%@page import="anchormen.platform3.security.web.Authenticatable"
%><%@page import="anchormen.platform3.util.*"
%><%@page import="anchormen.java.util.Strings"
%><%@page import="anchormen.java.collections.Enumerable"
%><%@page import="anchormen.java.collections.Meta"
%><%@page import="java.util.*"
%><%@taglib uri="/am" prefix="am"
%><%
	AbstractMsfPage p = getCurrentPage();
	Authenticatable user = p.getAuthenticatable(); 
	boolean useDropdowns = true;
%>
<nav class="navbar navbar-default">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<a class="navbar-brand" href="#">MSF Tool</a>
		</div>
		
		<div class="login-container pull-right">
			<div class="pull-right btn-group">
				<% if ( user == null ) { %>
					<am:page-link cssClass="btn btn-sm btn-success" pageclass="<%= LoginPage.class %>"><%= getString("login") %></am:page-link>
				<% } else { %>
					<am:page-link cssClass="btn btn-sm btn-default" pageclass="<%= UserProfilePage.class %>"><%= user.getName() %></am:page-link>
					<am:page-link cssClass="btn btn-sm btn-warning" pageclass="<%= LogoutPage.class %>"><%= getString("logout") %></am:page-link>
				<% } %>
			</div>
		</div>
		
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<%
				for (Meta<MenuItem> entry : Enumerable.<MenuItem>wrap(MenuItem.getMenu()).meta())
				{
					MenuItem menuItem = entry.getValue();
					boolean active = menuItem.isActive(p.getClass());
					boolean hasDropDown = useDropdowns && menuItem.hasVisibleSubItems(user);

					String cssClass = "menuitem";
					if ( entry.isLast() )
						cssClass += " last";
					if ( entry.isFirst() )
						cssClass += " first";
					if ( entry.isOdd() )
						cssClass += " odd";
					else
						cssClass += " even";
					if ( active )
						cssClass += " active";

					//if ( hasDropDown )
					//	cssClass += " dropdown-toggle";

					if ( menuItem.isVisible(user) ) 
					{
						%>
						<li class="<%= cssClass %> <% if ( hasDropDown ) { %> dropdown <% }%>">
							<% if ( !hasDropDown ) { %>
								<a<%=menuItem.getNewPage()?" target=\"_blank\"":""%> class="pagelink"<% if ( menuItem.getUseLink() ) { %> href="<%= PageLinkUtil.pageActionLink(menuItem.getMainPage(), menuItem.getAction(), null) %>"<% } %>>
									<%= Strings.xmlEncode(getString(menuItem.getLanguageKey()))%>
								</a>
							<% } else { %>
								<a class="dropdown-toggle<%=(menuItem.getUseLink()?" disabled":"")%>"<% if ( menuItem.getUseLink() ) { %> href="<%= PageLinkUtil.pageActionLink(menuItem.getMainPage(), menuItem.getAction(), null) %>"<% } else { %> href="#"<%}%> data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<%= Strings.xmlEncode(getString(menuItem.getLanguageKey()))%> <span class="caret"></span>
								</a>
								<ul class="dropdown-menu">
									<% for ( MenuItem subItem : menuItem.getSubItems() ) { %>
										<% if ( subItem.isVisible(user) ) { %>
											<% if ( subItem.getMainPage() == null ) { %>
												<li role="separator" class="divider"></li>
											<% } else { %>
												<li class="<%= subItem.isActive(p.getClass())?"active":"" %>">
													<a<%=subItem.getNewPage()?" target=\"_blank\"":""%> class="pagelink" href="<%= PageLinkUtil.pageActionLink(subItem.getMainPage(), subItem.getAction(), null) %>">
														<%= Strings.xmlEncode(getString(subItem.getLanguageKey())) %>
													</a>
												</li>
											<% } %>
										<% } %>
									<% } %>
								</ul>

							<% } %>
						</li>
						<%
					}
				}
				%>
			</ul>
		</div>
	</div>
</nav>

